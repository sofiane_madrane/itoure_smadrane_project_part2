# Introduction to language theory and compiling Project – Part2

This is the second part of the project, consisting in implementing a parser for the Imp language



## Context

For this second part of the project, you will write the parser of your IMP compiler. More precisely,
you must:
1. Transform the IMP grammar (see Figure 1) in order to: (a) Remove unreachable and/or unproductive
variables, if any; (b) Make the grammar non-ambiguous by taking into account the priority
and the associativity of the operators. Table 1 shows these priorities and associativities: operators
are sorted by decreasing order of priority (with two operators in the same row having the same
priority); (c) Remove left-recursion and apply factorisation where need be.
2. Give the action table of an LL(1) parser for the transformed grammar. You must justify this table
by giving the details of the computations of the relevant First and Follow sets.
3. Write, in Java, a recursive descent LL(1) parser for this grammar. Your parser should preferably
use the scanner that you have designed in the first part of the project in order to extract the sequence
of tokens from the input. For this part of the project, the output of your parser must consist only
of the leftmost derivation of the input string, if it is correct; or an error message if there is a syntax
error. The leftmost derivation can be given as a sequence of rule numbers (do not forget to number
your rules accordingly in the report!).
4. (Bonus) Your program could also print the corresponding parse tree.
Note: You must implement your parser from scratch. You are not allowed to use tools such as yacc. For
the lexer, you can either (preferably) use the one you designed in Part 1, or use the one available online
from Monday, November 6th.
You must hand in:
 A PDF report containing the modified grammar, the action table, with all the necessary justifications,
choices and hypotheses;
 The source code of your parser;
 The IMP example files you have used to test your analyser;
 All required files to evaluate your work (like a Main.java file calling the parser, etc).

![figure1 IMP grammar](https://image.noelshack.com/fichiers/2019/46/3/1573654490-figure1impgrammar.png)


| Operators  | Associativity    |       
|:---------------: |:---------------:| 
| -, not  |   right        |  
| *, / | left             |   
| +, - | left          |    
| >, <, >=, <=, =, <> | left          |  
| and |left          |  
| or |left          |  

Table 1: Priority and associativity of the IMP operators (operators are sorted in decreasing order of
priority).


You must structure your files in five folders

* **doc** contains the JAVADOC and the PDF report
* **test** contains all your example files
* **dist** contains an executable JAR that must be called part2.jar
* **src** contains your source files
* **more** contains all other files


Your implementation must contain:
1. your scanner (from the first part of the project);
2. your parser;
3. an executable public class Main that reads the file given as argument and writes on the standard
output stream the leftmost derivation.



## Running

The command for running your executable must be:

java -jar part2.jar sourceFile

## Design with

Entrez les programmes/logiciels/ressources que vous avez utilisé pour développer votre projet

_exemples :_
* [Java](https://www.java.com) - Java 1.6 
* [Atom](https://atom.io/) - Editeur de textes
* [JFlex](https://jflex.de/)  - JFlex is a lexical analyzer generator  (1.6.1)


## Authors
Listez le(s) auteur(s) du projet ici !
* **Sofiane Madrane** _alias_ [@sofiane_madrane](https://gitlab.com/sofiane_madrane)




