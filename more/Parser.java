import java.util.ArrayList;

/**
 * This class represents the logic of parser.
 *
 * @author  I.Toure, S.Madrane,
 * @version 1.0
 */
public class Parser {
    // Array containing all the rules of the grammar
    private static final String[] rules = {
            "[1]  <Program>         --> begin <Code> end",
            "[2]  <Code>            --> ε",
            "[3]  <Code>            --> <InstList>",
            "[4]  <InstList>        --> <Instruction> <InstList'> ",
            "[5]  <InstList'>       --> ; <InstList>",
            "[6]  <InstList'>       --> ε",
            "[7]  <Instruction>     --> <Assign>",
            "[8]  <Instruction>     --> <If>",
            "[9]  <Instruction>     --> <While>",
            "[10] <Instruction>     --> <For>",
            "[11] <Instruction>     --> <Print>",
            "[12] <Instruction>     --> <Read>",
            "[13] <Assign>          --> [VarName] := <ExprArith>",
            "[14] <ExprArith>       --> <Prod> <ExprArith'>",
            "[15] <ExprArith'>      --> + <Prod> <ExprArith'>",
            "[16] <ExprArith'>      --> - <Prod> <ExprArith'>",
            "[17] <ExprArith'>      --> ε",
            "[18] <Prod>            --> <Atom> <Prod'>",
            "[19] <Prod'>           --> * <Atom> <Prod'>",
            "[20] <Prod'>           --> / <Atom> <Prod'>",
            "[21] <Prod'>           --> ε",
            "[22] <Atom>            --> - <Atom>",
            "[23] <Atom>            --> [VarName]",
            "[24] <Atom>            --> [Number]",
            "[25] <Atom>            --> ( <ExprArith> )",
            "[26] <If>              --> if <Cond> then <Code> <If'>",
            "[27] <If'>             --> endif",
            "[28] <If'>             --> else <Code> endif",
            "[29] <Cond>            --> <BooleanTerm> <Cond'>",
            "[30] <Cond'>           --> or <BooleanTerm> <Cond'>",
            "[31] <Cond'>           --> ε",
            "[32] <BooleanTerm>     --> <BooleanFactor> <BooleanTerm'>",
            "[33] <BooleanTerm'>    --> and <BooleanFactor> <BooleanTerm'>",
            "[34] <BooleanTerm'>    --> ε",
            "[35] <BooleanFactor>   --> not <SimpleCond>",
            "[36] <BooleanFactor>   --> <SimpleCond>",
            "[37] <SimpleCond>      --> <SimpleCond>",
            "[38] <SimpleCond>      --> <ExprArith> <Comp> <ExprArith>",
            "[39] <Comp>            --> =",
            "[40] <Comp>            --> >=",
            "[41] <Comp>            --> >",
            "[42] <Comp>            --> <=",
            "[43] <Comp>            --> <",
            "[44] <Comp>            --> <>",
            "[45] <While>           --> while <Cond> do <Code> done",
            "[46] <For>             --> for [VarName] from <ExprArith> <For'>",
            "[47] <For'>            --> by <ExprArith> to <ExprArith> do <Code> done",
            "[48] <Print>           --> print([VarName])",
            "[49] <Read>            --> read([VarName])",};
    // Tokens of the input file to parse
    private final ArrayList<Symbol> tokenTable;
    // Index on the List of token of the current one
    private int indexCurrentToken;
    private Symbol currentToken;

    /**
     * @param tokenTable List of token of the input file
     */
    public Parser(ArrayList<Symbol> tokenTable) {
        indexCurrentToken = 0;
        this.tokenTable = tokenTable;
    }

    /**
     * parse the token of the input file
     *
     * @throws ParserException if a syntax error happen during the parsing of the input file
     */
    public void parse() throws ParserException {
        if (!tokenTable.isEmpty()) {
            currentToken = tokenTable.get(indexCurrentToken);
            Program();
            System.out.println("The input file is syntactically correct");

        } else {
            throw new ParserException(1);
        }
    }

    /**
     * Match the current token LexicalUnit with the one that has been derivated
     * from the rules of the grammar
     *
     * @param unit LexicalUnit that has been derivated from the grammar
     * @param indexRules index of the rules that has made the call
     * @throws ParserException if any LexicalUnit mismatch happen
     */
    private void match(LexicalUnit unit, int indexRules) throws ParserException {
        if (indexCurrentToken == tokenTable.size()) {
            throw new ParserException(indexRules);
        }
        if (currentToken.getType() != unit) {
            throw new ParserException(indexRules);
        } else {
            indexCurrentToken++;
            if (indexCurrentToken < tokenTable.size()) {
                currentToken = tokenTable.get(indexCurrentToken);
            }
        }
    }

    /**
     * Show on the ouput the rule referenced by the index
     *
     * @param index index of the rule that need to be printed
     */
    private void printRuleString(int index) {
        System.out.println(rules[index - 1]);
    }

    /**
     * Represent the variable Program of the grammar
     *
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Program() throws ParserException {
        switch (currentToken.getType()) {
            case BEGIN:
                printRuleString(1);
                match(LexicalUnit.BEGIN, 1);
                Code(1);
                match(LexicalUnit.END, 1);
                if (indexCurrentToken != tokenTable.size()) {
                    throw new ParserException("Expected 'END' at the end of the input");
                }
                break;
            default:
                throw new ParserException(1);
        }
    }

    /**
     * Represent the variable Code of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Code(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case END:
            case ENDIF:
            case ELSE:
            case DONE:
                printRuleString(2);
                break;
            case VARNAME:
            case IF:
            case WHILE:
            case FOR:
            case PRINT:
            case READ:
                printRuleString(3);
                InstList(3);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable InstList of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void InstList(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case VARNAME:
            case WHILE:
            case PRINT:
            case IF:
            case READ:
            case FOR:
                printRuleString(4);
                Instruction(4);
                InstListPrime(4);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable InstListPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void InstListPrime(int rule) throws ParserException {
        switch (currentToken.getType()){
            case SEMICOLON:
                printRuleString(5);
                match(LexicalUnit.SEMICOLON,5);
                InstList(rule);
                break;
            case END:
            case ENDIF:
            case ELSE:
            case DONE:
                printRuleString(6);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Instruction of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Instruction(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case VARNAME:
                printRuleString(7);
                Assign(7);
                break;
            case WHILE:
                printRuleString(9);
                While(9);
                break;
            case PRINT:
                printRuleString(11);
                Print(11);
                break;
            case IF:
                printRuleString(8);
                If(8);
                break;
            case READ:
                printRuleString(12);
                Read(12);
                break;
            case FOR:
                printRuleString(10);
                For(10);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Assign of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Assign(int rule) throws ParserException {
        switch (currentToken.getType()){
            case VARNAME:
                printRuleString(13);
                match(LexicalUnit.VARNAME, 13);
                match(LexicalUnit.ASSIGN, 13);
                ExprArith(13);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable ExprArith of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void ExprArith(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case VARNAME:
            case MINUS:
            case NUMBER:
            case LPAREN:
                printRuleString(14);
                Prod(14);
                ExprArithPrime(14);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable ExprArithPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void ExprArithPrime(int rule) throws ParserException {
        switch (currentToken.getType()){
            case PLUS:
                printRuleString(15);
                match(LexicalUnit.PLUS,15);
                Prod(15);
                ExprArithPrime(15);
                break;
            case MINUS:
                printRuleString(16);
                match(LexicalUnit.MINUS,16);
                Prod(16);
                ExprArithPrime(16);
                break;
            case SEMICOLON:
            case LPAREN:
            case RPAREN:
            case THEN:
            case END:
            case ENDIF:
            case ELSE:
            case OR:
            case AND:
            case EQ:
            case GEQ:
            case GT:
            case LEQ:
            case LT:
            case NEQ:
            case DO:
            case DONE:
            case BY:
            case TO:
                printRuleString(17);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Prod of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Prod(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case VARNAME:
            case MINUS:
            case NUMBER:
            case LPAREN:
                printRuleString(18);
                Atom(18);
                ProdPrime(18);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable ProdPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void ProdPrime(int rule) throws ParserException {
        switch (currentToken.getType()){
            case TIMES:
                printRuleString(19);
                match(LexicalUnit.TIMES, 19);
                Atom(19);
                ProdPrime(19);
                break;
            case DIVIDE:
                printRuleString(20);
                match(LexicalUnit.DIVIDE, 20);
                Atom(20);
                ProdPrime(20);
                break;
            case MINUS:
            case PLUS:
            case ENDIF:
            case ELSE:
            case OR:
            case AND:
            case EQ:
            case GEQ:
            case GT:
            case LEQ:
            case LT:
            case NEQ:
            case BY:
            case TO:
            case RPAREN:
            case END:
            case DONE:
            case SEMICOLON:
            case DO:
            case THEN:
                printRuleString(21);
                break;

            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Atom of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Atom(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case VARNAME:
                printRuleString(24);
                match(LexicalUnit.VARNAME, 24);
                break;
            case MINUS:
                printRuleString(22);
                match(LexicalUnit.MINUS, 22);
                Atom(22);
                break;
            case NUMBER:
                printRuleString(23);
                match(LexicalUnit.NUMBER, 23);
                break;
            case LPAREN:
                printRuleString(25);
                match(LexicalUnit.LPAREN, 25);
                ExprArith(25);
                match(LexicalUnit.RPAREN, 25);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable If of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void If(int rule) throws ParserException {
        switch (currentToken.getType()){
            case IF:
                printRuleString(26);
                match(LexicalUnit.IF, 26);
                Cond(26);
                match(LexicalUnit.THEN, 26);
                Code(26);
                IfPrime(26);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable IfPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void IfPrime(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case ENDIF:
                printRuleString(27);
                match(LexicalUnit.ENDIF, 27);
                break;
            case ELSE:
                printRuleString(28);
                match(LexicalUnit.ELSE, 28);
                Code(28);
                match(LexicalUnit.ENDIF, 28);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Cond of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Cond(int rule) throws ParserException {
        switch (currentToken.getType()){
            case VARNAME:
            case MINUS:
            case NUMBER:
            case RPAREN:
            case NOT:
                printRuleString(29);
                BooleanTerm(29);
                CondPrime(29);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable CondPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void CondPrime(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case OR:
                printRuleString(30);
                match(LexicalUnit.OR, 30);
                BooleanTerm(30);
                CondPrime(30);
                break;
            case DO:
            case THEN:
                printRuleString(31);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable BooleanTerm of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void BooleanTerm(int rule) throws ParserException {
        switch (currentToken.getType()){
            case VARNAME:
            case MINUS:
            case NUMBER:
            case RPAREN:
            case NOT:
                printRuleString(32);
                BooleanFactor(32);
                BooleanTermPrime(32);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable BooleanTermPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void BooleanTermPrime(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case OR:
            case DO:
            case THEN:
                printRuleString(34);
                break;
            case AND:
                printRuleString(33);
                match(LexicalUnit.AND, 33);
                BooleanFactor(33);
                BooleanTermPrime(33);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable BooleanFactor of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void BooleanFactor(int rule) throws ParserException {
        switch (currentToken.getType()){
            case NOT:
                printRuleString(35);
                match(LexicalUnit.NOT, 35);
                SimpleCond(35);
                break;
            case VARNAME:
            case MINUS:
            case NUMBER:
            case RPAREN:
                printRuleString(36);
                SimpleCond(36);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable SimpleCond of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void SimpleCond(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case VARNAME:
            case MINUS:
            case NUMBER:
            case LPAREN:
                printRuleString(37);
                ExprArith(37);
                Comp(37);
                ExprArith(37);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Comp of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Comp(int rule) throws ParserException {
        switch (currentToken.getType()){
            case EQ:
                printRuleString(38);
                match(LexicalUnit.EQ,38);
                break;
            case GEQ:
                printRuleString(39);
                match(LexicalUnit.GEQ,39);
                break;
            case GT:
                printRuleString(40);
                match(LexicalUnit.GT, 40);
                break;
            case LEQ:
                printRuleString(41);
                match(LexicalUnit.LEQ, 41);
                break;
            case LT:
                printRuleString(42);
                match(LexicalUnit.LT, 42);
                break;
            case NEQ:
                printRuleString(43);
                match(LexicalUnit.NEQ, 43);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable While of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void While(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case WHILE:
                printRuleString(44);
                match(LexicalUnit.WHILE, 44);
                Cond(44);
                match(LexicalUnit.DO, 44);
                Code(44);
                match(LexicalUnit.DONE, 44);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable ForPrime of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void ForPrime(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case BY:
                printRuleString(46);
                match(LexicalUnit.BY, 46);
                ExprArith(46);
                match(LexicalUnit.TO, 46);
                ExprArith(46);
                match(LexicalUnit.DO, 46);
                Code(46);
                match(LexicalUnit.DONE, 46);
                break;
            case TO:
                printRuleString(47);
                match(LexicalUnit.TO, 47);
                ExprArith(47);
                match(LexicalUnit.DO, 47);
                Code(47);
                match(LexicalUnit.DONE, 47);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable For of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void For(int rule) throws ParserException {
        switch (currentToken.getType()){
            case FOR:
                printRuleString(45);
                match(LexicalUnit.FOR, 45);
                match(LexicalUnit.VARNAME, 45);
                match(LexicalUnit.FROM, 45);
                ExprArith(45);
                ForPrime(45);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Print of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Print(int rule) throws ParserException {
        switch (currentToken.getType()){
            case PRINT:
                printRuleString(48);
                match(LexicalUnit.PRINT, 48);
                match(LexicalUnit.LPAREN, 48);
                match(LexicalUnit.VARNAME, 48);
                match(LexicalUnit.RPAREN, 48);
                break;
            default:
                throw new ParserException(rule);
        }
    }

    /**
     * Represent the variable Read of the grammar
     *
     * @param rule number of the rule that has made the call to this one
     * @throws ParserException if any syntax error happen on the current rule
     */
    private void Read(int rule) throws ParserException {
        switch (currentToken.getType()) {
            case READ:
                printRuleString(49);
                match(LexicalUnit.READ, 49);
                match(LexicalUnit.LPAREN, 49);
                match(LexicalUnit.VARNAME, 49);
                match(LexicalUnit.RPAREN, 49);
                break;
            default:
                throw new ParserException(rule);
        }
    }
}

