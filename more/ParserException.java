/**
 * This class an thrown exception   when the parser encounters an error
 *
 * @author S.Madrane, I.Toure
 * @version 1.0
 */

public class ParserException extends java.io.IOException {
    /**
     * Displays the error message due to the exception.
     *
     * @param indexRules representing the error produced.
     */
    public ParserException(int indexRules) {
        super("[SyntaxParserException] A syntax error occurs at rule [" + indexRules + "]");
    }

    /**
     * Displays the error message due to the exception
     *
     * @param message message about the error
     */
    public ParserException(String message) {
        super(message);
    }
}
