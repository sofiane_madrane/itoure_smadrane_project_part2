import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class is the main class of the IMP project and launches the parser.
 * @author I.Toure, S.Madrane
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("No input File");
        } else {
            try {
                LexicalAnalyzer lex = new LexicalAnalyzer(new BufferedReader(
                        new FileReader(args[0])));
                while (!lex.eof()) {
                    lex.next_token();
                }
                Parser parser = new Parser(lex.getTokenTable());
                parser.parse();
            } catch (ParserException e){
                System.out.println(e.getMessage());
            }  catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
